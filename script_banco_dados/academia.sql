-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 28-Maio-2020 às 22:13
-- Versão do servidor: 5.7.24
-- versão do PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `academia`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula`
--

DROP TABLE IF EXISTS `aula`;
CREATE TABLE IF NOT EXISTS `aula` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `Sala` varchar(45) DEFAULT NULL,
  `Instrutor_ID` int(11) NOT NULL,
  `TipoAtividade_ID` int(11) NOT NULL,
  `HoraInicio` time DEFAULT NULL,
  `HoraFim` time DEFAULT NULL,
  `Domingo` tinyint(4) DEFAULT NULL,
  `Segunda` tinyint(4) DEFAULT NULL,
  `Terca` tinyint(4) DEFAULT NULL,
  `Quarta` tinyint(4) DEFAULT NULL,
  `Quinta` tinyint(4) DEFAULT NULL,
  `Sexta` tinyint(4) DEFAULT NULL,
  `Sabado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliaçãofisica`
--

DROP TABLE IF EXISTS `avaliaçãofisica`;
CREATE TABLE IF NOT EXISTS `avaliaçãofisica` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario_ID` int(11) NOT NULL,
  `Anamnese` text,
  `Data` date DEFAULT NULL,
  `DobrasCutaneas` text,
  `Ergometrico` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ferias`
--

DROP TABLE IF EXISTS `ferias`;
CREATE TABLE IF NOT EXISTS `ferias` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Plano_ID` int(11) NOT NULL,
  `DataInicial` date DEFAULT NULL,
  `DataFinal` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `instrutor`
--

DROP TABLE IF EXISTS `instrutor`;
CREATE TABLE IF NOT EXISTS `instrutor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) DEFAULT NULL,
  `CPF` varchar(45) DEFAULT NULL,
  `RG` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `instrutor`
--

INSERT INTO `instrutor` (`ID`, `Nome`, `CPF`, `RG`) VALUES
(1, 'André Guerra', '85590746434', '4009307');

-- --------------------------------------------------------

--
-- Estrutura da tabela `instrutortipoatividade`
--

DROP TABLE IF EXISTS `instrutortipoatividade`;
CREATE TABLE IF NOT EXISTS `instrutortipoatividade` (
  `Instrutor_ID` int(11) NOT NULL,
  `TipoAtividade_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `pagamento`
--

DROP TABLE IF EXISTS `pagamento`;
CREATE TABLE IF NOT EXISTS `pagamento` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Plano_ID` int(11) NOT NULL,
  `Valor` float DEFAULT NULL,
  `DataVencimento` date DEFAULT NULL,
  `DataPagamento` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `pagamento`
--

INSERT INTO `pagamento` (`ID`, `Plano_ID`, `Valor`, `DataVencimento`, `DataPagamento`) VALUES
(1, 1, 200, '2020-01-01', '2020-01-01'),
(2, 1, 200, '2020-02-01', '2020-02-01'),
(3, 1, 200, '2020-03-01', NULL),
(4, 1, 200, '2020-04-01', NULL),
(5, 1, 200, '2020-05-01', NULL),
(6, 1, 200, '2020-06-01', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `planousuario`
--

DROP TABLE IF EXISTS `planousuario`;
CREATE TABLE IF NOT EXISTS `planousuario` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Preço` float DEFAULT NULL,
  `Usuario_ID` int(11) NOT NULL,
  `Tipo` int(11) DEFAULT NULL,
  `DataInicio` date DEFAULT NULL,
  `DataFim` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `planousuario`
--

INSERT INTO `planousuario` (`ID`, `Preço`, `Usuario_ID`, `Tipo`, `DataInicio`, `DataFim`) VALUES
(1, 1200, 1, 1, '2020-01-01', '2021-01-01');

-- --------------------------------------------------------

--
-- Estrutura da tabela `registroatividade`
--

DROP TABLE IF EXISTS `registroatividade`;
CREATE TABLE IF NOT EXISTS `registroatividade` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Data` date DEFAULT NULL,
  `Usuario_ID` int(11) NOT NULL,
  `Aula_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipoatividade`
--

DROP TABLE IF EXISTS `tipoatividade`;
CREATE TABLE IF NOT EXISTS `tipoatividade` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) DEFAULT NULL,
  `Login` varchar(45) DEFAULT NULL,
  `Senha` varchar(45) DEFAULT NULL,
  `AutenticacaoBiometrica` text,
  `TipoUsuario` int(11) DEFAULT NULL,
  `Matricula` varchar(45) DEFAULT NULL,
  `RG` varchar(45) DEFAULT NULL,
  `CPF` varchar(45) DEFAULT NULL,
  `Endereco` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`ID`, `Nome`, `Login`, `Senha`, `AutenticacaoBiometrica`, `TipoUsuario`, `Matricula`, `RG`, `CPF`, `Endereco`) VALUES
(1, 'Maria da Silva', 'msilva', 'msilva2020', NULL, 1, '21323232', '4009435', '855.907.464-34', 'Rua das Creoulas, 456 - Madalena - Recife - PE\r\nCEP 50.610-150');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
